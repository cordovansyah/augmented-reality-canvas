# Augmented Reality Canvas

Augmented Reality Canvas ( AR Canvas ) is an ARKit technology based iOS app that allows everyone to express their creativity beyond the tip of their tablet or paper, which indeed through augmented reality world.

# Motivation
As part of my current career in Octagon - an AR Software Company in Bandung, Indonesia - , I am really hyped to these virtual based technologies and push myself to explore even further and deeper into the 
AR universe in iOS development. Honestly, it's really fun to learn how AR development works, the advantage of ARKit 2.0 module and to see it happens in front of my very own eyes, it's amazing. I just, hoenstly,
just love it. To me, AR is a counterpart of future society interaction. To know how to build one, and also enhancing it's experience, it's amazing. Love it !


## Why I Build This App?
First of all, it leans with my current professional career and secondly, AR is somewhat magic to most of the people, including my family as well. Thus, I really wanna show them that I can build one that is useful and also fun,
specifically for my sisters. You may be overwhelmed with how many fun words spilled out in this readme file, but it's the honest, swear to god it's amazing. I wanna keep building more, more and more.

## Demonstration
I put all of my app demonstration for screening purpose on a Youtube playlist just to make things clear


### Motivation

### Prerequisites
```
Xcode 10.0
Swift 4.0 / 4.2 
iOS 12
```

## Built With

* [MNIST Digit Classifier](http://yann.lecun.com/exdb/mnist/) - CoreML Model Used
* [CoreML Model Libraries](https://coreml.store/) - CoreML Model Used


## Acknowledgments
* Amazing CoreML Model Libraries !! (https://coreml.store/)

